# Training task for Laravel + Docker by ASPER Brothers

The training task is:
- to prepare an environment based on Docker containers, 
- creating an application based on the Laravel framework, 
- which will use a method of CLI to import data from a sample Excel file (xlsx) into a relational database MySQL 
- and then send an email to the administrator with the status (how many records have been added, 
  how many records have been added, how many updated).

The resulting app exposes front end with a filtered table

1. City,
2. Available streets in the city,
3. Available years,
4. Finally, we show the objects.

Proposed schema contains tables named:

- cities, 
- streets, 
- years, 
- objects

The code should be covered by unit tests and delivered in the form of a repository GIT.

## Run the development version

    # Clone repository
    git clone git@bitbucket.org:asperdevs/pride.git
    cd pride

    # build and start 
    docker-compose up --build --detach
    
    # Allow www-data user to save logs
    sudo chmod -R o+w ./storage/

## provision docker container
 
    # Go to php-fpm container
    docker-compose exec app bash

    # Continue from inside container

    # generate .env and fill in values
    cp .env.example .env

    # Install dependencies
    composer install

    # Generate app secret key
    php artisan key:generate

    # Create database schema
    php artisan migrate:fresh

    # Install npm dependencies
    npm install

    # Compile assets
    npm run dev

    # Optionally import example data
    php artisan cities:save testdata.xlsx

## Tests

- CRUD operations via API
- CLI command using testdata.xlsx
- repository operations
- sending admin emails

To run tests:

    docker-compose exec app phpunit
