@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px;">
                <a href="{{ route('cities.import') }}" class="btn btn-primary text-left">Import Excel file</a>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card text-center">
                    <div class="card-body">
                        <cities-table :cities='{!! json_encode($cities) !!}'></cities-table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
