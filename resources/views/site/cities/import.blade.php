@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-center">
                    <div class="card-header">Import Excel file</div>

                    <div class="card-body">
                        <form style="border: 1px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('cities.import.save') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                            @csrf

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if (Session::has('success'))
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <p>{{ Session::get('success') }}</p>
                                </div>
                            @endif

                            <input type="file" name="import_file" />
                            <button class="btn btn-primary">Import file</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
