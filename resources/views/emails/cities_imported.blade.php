@component('mail::message')

    # Records from the file has been successfuly imported.

    Statistics:

    Added records: {{ $added_records }}
    Modified records: {{ $modified_records }}

@endcomponent
