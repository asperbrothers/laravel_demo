<?php

Route::get('/', 'Site\CitiesController@index')->name('cities.index');
Route::get('/cities/import', 'Site\CitiesController@import')->name('cities.import');
Route::post('/cities/import/save', 'Site\CitiesController@importSave')->name('cities.import.save');
