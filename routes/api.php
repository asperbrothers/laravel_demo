<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/streets/{city_id}', 'Api\StreetsController@getByCity')->name('streets.get_by_city');
Route::put('/streets/{street}', 'Api\StreetsController@update')->name('streets.update');
Route::delete('/streets/{street}', 'Api\StreetsController@delete')->name('streets.delete');

Route::get('/years/{street_id}', 'Api\YearsController@getByStreet')->name('years.get_by_street');
Route::put('/years/{year}', 'Api\YearsController@update')->name('years.update');
Route::delete('/years/{year}', 'Api\YearsController@delete')->name('years.delete');

Route::get('/buildings', 'Api\BuildingsController@getBuildings')->name('buildings.get_buildings');
Route::put('/buildings/{building}', 'Api\BuildingsController@update')->name('buildings.update');
Route::delete('/buildings/{building}', 'Api\BuildingsController@delete')->name('buildings.delete');

