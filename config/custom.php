<?php

return [
    'admin_email' => env('ADMIN_EMAIL', 'adrian.affek1@gmail.com'),
    'imports_folder' => env('IMPORTS_FOLDER', 'files')
];