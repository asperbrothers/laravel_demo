<?php

namespace Tests\Feature\Http\Site;

use App\Model\Building;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CitiesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIfSavingDataFromGivenFile()
    {
        $files_before_adding = scandir(public_path() . '/' . config('custom.imports_folder'));
        $files_before_adding = array_diff($files_before_adding, array('.', '..'));

        $response = $this->post('/cities/import/save', [
            'import_file' => new \Illuminate\Http\UploadedFile(public_path() . '/' . config('custom.imports_folder') . '/' . 'testdata.xlsx',
                'testdata.xlsx', null, null,true)
        ]);

        $buildings = Building::all();

        $files_after_adding = scandir(public_path() . '/' . config('custom.imports_folder'));
        $files_after_adding = array_diff($files_after_adding, array('.', '..'));

        usort($files_after_adding, function($a, $b){
            return filectime(public_path() . '/' . config('custom.imports_folder') . '/' . $a) < filectime( public_path() . '/' . config('custom.imports_folder') . '/' .  $b);
        });

        Storage::disk('imports')->delete('', $files_after_adding[0]);

        $this->assertCount(count($files_before_adding) + 1, $files_after_adding);
        $this->assertNotEmpty($buildings);
        $this->assertCount($buildings->count(), $buildings);
        $this->assertEquals(0, count(Mail::failures()));
    }
}
