<?php

namespace Tests\Feature\Http\Api;

use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class YearsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIfItWillGetYearByStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $response = $this->get("/api/years/{$street->id}");
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertIsArray($data);
        $this->assertNotEmpty($data['data']);
    }

    public function testIfItsPossibleToUpdateStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $response = $this->put("/api/years/{$year->id}", ['year' => '1999']);
        $response->assertStatus(200);

        $newYearFromDb = Year::find($year->id);

        $this->assertNotEquals($year->year, $newYearFromDb->year);
    }

    public function testIfItsPossibleToDeleteStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $response = $this->delete("/api/years/{$year->id}");
        $response->assertStatus(200);

        $this->assertDatabaseMissing('years', ['id' => $year->id]);
    }
}