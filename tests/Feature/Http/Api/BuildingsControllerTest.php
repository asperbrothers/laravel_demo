<?php

namespace Tests\Feature\Http\Api;

use App\Model\Building;
use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BuildingsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIfWillReturnBuildings()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $params = http_build_query(['city' => $city->id, 'street' => $street->id, 'year' => $year->id]);

        $response = $this->get('/api/buildings?' . $params);
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertIsArray($data);
        $this->assertCount(1, $data['data']);
        $this->assertArrayHasKey('building_name', $data['data'][0]);
    }

    public function testIfItsPossibleToUpdateAnyRecord()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $response = $this->put("/api/buildings/{$building->id}", ['name' => 'test name']);
        $response->assertStatus(200);

        $newBuildingFromDb = Building::find($building->id);

        $this->assertNotEquals($building->name, $newBuildingFromDb->name);
    }

    public function testIfItsPossibleToDeleteBuilding()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $response = $this->delete("/api/buildings/{$building->id}");
        $response->assertStatus(200);

        $this->assertDatabaseMissing('buildings', ['id' => $building->id]);
    }
}
