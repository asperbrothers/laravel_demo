<?php

namespace Tests\Feature\Http\Api;

use App\Model\City;
use App\Model\Street;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StreetsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIfItWillGetStreetsByCity()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Testowa 1',
            'city_id' => $city->id
        ]);

        $response = $this->get("/api/streets/{$city->id}");
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertIsArray($data);
        $this->assertNotEmpty($data['data']);
    }

    public function testIfItsPossibleToUpdateStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $response = $this->put("/api/streets/{$street->id}", ['name' => 'test name']);
        $response->assertStatus(200);

        $newStreetFromDb = Street::find($street->id);

        $this->assertNotEquals($street->name, $newStreetFromDb->name);
    }

    public function testIfItsPossibleToDeleteStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $response = $this->delete("/api/streets/{$street->id}");
        $response->assertStatus(200);

        $this->assertDatabaseMissing('streets', ['id' => $street->id]);
    }
}
