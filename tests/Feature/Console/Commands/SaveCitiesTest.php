<?php

namespace Tests\Feature\Console\Commands;

use App\Model\Building;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SaveCitiesTest extends TestCase
{
    use RefreshDatabase;

    public function testIfCommandSaveDataFromFileAndSendEmailToAdmin()
    {
        Artisan::call('cities:save', [
            'filename' => 'testdata.xlsx'
        ]);

        $buildings = Building::all();

        $this->assertNotEmpty($buildings);
        $this->assertCount($buildings->count(), $buildings);
        $this->assertEquals(0, count(Mail::failures()));
    }
}
