<?php

namespace Tests\Unit\Imports;

use App\Imports\CitiesImport;
use Tests\TestCase;

class CitiesImportTest extends TestCase
{
    public function testIfCollectionMethodWillReturnProperData()
    {
        $citiesImport = new CitiesImport();

        $this->assertIsArray($citiesImport->collection(collect(['test'])));
    }
}