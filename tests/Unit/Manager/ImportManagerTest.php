<?php

namespace Tests\Unit\Manager;

use App\Dto\XlsxDto;
use App\Exceptions\InvalidFileStructureFormatException;
use App\Manager\ImportManager;
use App\Model\City;
use App\Model\Building;
use App\Model\Street;
use App\Model\Year;
use App\Repository\CityRepository;
use App\Repository\BuildingRepository;
use App\Repository\StreetRepository;
use App\Repository\YearRepository;
use App\Validator\XlsxStructureValidator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImportManagerTest extends TestCase
{
    use RefreshDatabase;

    public function testIfManagerWillSaveDataToDatabase()
    {
        $data = [
            'miasto' => 'Warszawa',
            'ulica' => 'Jana Pankiewicza',
            'nazwa_obiektu' => 'Biuro',
            'rok' => '2019'
        ];

        $manager = new ImportManager(
            new CityRepository(new City),
            new StreetRepository(new Street),
            new YearRepository(new Year),
            new BuildingRepository(new Building),
            new XlsxStructureValidator(),
            new XlsxDto()
        );

        $isNewRecordsAdded = $manager->save($data);

        $buildings = Building::all();

        $this->assertNotEmpty($buildings);
    }

    public function testIfManagerWillNotSaveDataIfSameRecordAlreadyExists()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2019',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $data = [
            'miasto' => 'Warszawa',
            'ulica' => 'Jana Pankiewicza',
            'nazwa_obiektu' => 'Biuro',
            'rok' => '2019'
        ];

        $manager = new ImportManager(
            new CityRepository(new City),
            new StreetRepository(new Street),
            new YearRepository(new Year),
            new BuildingRepository(new Building),
            new XlsxStructureValidator(),
            new XlsxDto()
        );

        $isNewRecordsAdded = $manager->save($data);

        $buildings = Building::all();

        $this->assertFalse($isNewRecordsAdded);
        $this->assertCount(1, $buildings);
    }

    public function testIfManagerWillCreateNewRecordIfDataStructureWillNotBeValid()
    {
        $invalid_data = [
            'city' => 'Warszawa',
            'ulica' => 'Jana Pankiewicza',
            'nazwa_obiektu' => 'Biuro',
            'rok' => '2019'
        ];

        $manager = new ImportManager(
            new CityRepository(new City),
            new StreetRepository(new Street),
            new YearRepository(new Year),
            new BuildingRepository(new Building),
            new XlsxStructureValidator(),
            new XlsxDto()
        );

        $this->expectException(InvalidFileStructureFormatException::class);

        $manager->save($invalid_data);
    }
}
