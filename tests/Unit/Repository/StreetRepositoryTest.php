<?php

namespace Tests\Unit\Repository;

use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use App\Repository\StreetRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StreetRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function testIfStreetWithGivenNameAndCityAlreadyExistsInDatabase()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $repo = new StreetRepository(new Street);

        $this->assertNotNull($repo->getIfExists('Jana Pankiewicza', $city));
    }

    public function testIfRepoWillReturnOnlyStreetsAssociatedToCity()
    {
        $first_city = City::create([
            'name' => 'Warszawa'
        ]);

        $second_city = City::create([
            'name' => 'Krakow'
        ]);

        Street::create([
            'name' => 'Testowa 1',
            'city_id' => $first_city->id
        ]);

        Street::create([
            'name' => 'Testowa 2',
            'city_id' => $first_city->id
        ]);

        Street::create([
            'name' => 'Testowa 3',
            'city_id' => $second_city->id
        ]);

        $repo = new StreetRepository(new Street);

        $this->assertCount(2, $repo->getByCityId($first_city->id));
    }

    public function testIfRepoCanCreateNewRecord()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $repo = new StreetRepository(new Street);

        $street = $repo->create(['street' => 'Testowa 1'], $city);

        $this->assertDatabaseHas('streets', ['id' => $street->id]);
    }

    public function testIfRepoCanUpdateStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Testowa 1',
            'city_id' => $city->id
        ]);

        $repo = new StreetRepository(new Street);

        $repo->update($street, ['name' => 'New name']);

        $this->assertEquals('New name', $street->name);
    }

    public function testIfRepoCanDeleteStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Testowa 1',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $repo = new StreetRepository(new Street);

        $this->assertTrue($repo->delete($street));
        $this->assertDatabaseMissing('streets', ['id' => $street->id]);
    }
}