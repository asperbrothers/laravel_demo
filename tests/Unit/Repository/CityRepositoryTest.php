<?php

namespace Tests\Unit\Repository;

use App\Model\City;
use App\Repository\CityRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function testIfItWillGetAllCitiesFromDatabase()
    {
        City::create([
            'name' => 'Warszawa'
        ]);

        City::create([
            'name' => 'Kraków'
        ]);

        $repo = new CityRepository(new City);

        $this->assertCount(2, $repo->getAll());
    }

    public function testIfCityWithGivenNameAlreadyExistsInDatabase()
    {
        City::create([
            'name' => 'Warszawa'
        ]);

        $repo = new CityRepository(new City);

        $this->assertNotNull($repo->getIfExists('Warszawa'));
    }

    public function testIfRepoCanCreateNewRecord()
    {
        $repo = new CityRepository(new City);

        $city = $repo->create([
            'city' => 'Warszawa'
        ]);

        $this->assertDatabaseHas('cities', ['id' => $city->id]);
    }
}