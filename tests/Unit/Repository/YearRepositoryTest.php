<?php

namespace Tests\Unit\Repository;

use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use App\Repository\YearRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class YearRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function testIfYearAssociatedToTheSameStreetAndCityExists()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $repo = new YearRepository(new Year);
        $yearExist = $repo->getIfExists($year->year, $city, $street);

        $this->assertInstanceOf(Year::class, $yearExist);
        $this->assertEquals($year->id, $yearExist->id);
    }

    public function testIfRepoCanCreateNewRecord()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $repo = new YearRepository(new Year);

        $year = $repo->create(['year' => '2019'], $street);

        $this->assertDatabaseHas('years', ['id' => $year->id]);
    }

    public function testIfRepoWillReturnYearsAssociatedToStreet()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $first_street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $second_street = Street::create([
            'name' => 'Testowa',
            'city_id' => $city->id
        ]);

        Year::create([
            'year' => '2000',
            'street_id' => $first_street->id
        ]);

        Year::create([
            'year' => '2001',
            'street_id' => $first_street->id
        ]);

        Year::create([
            'year' => '2009',
            'street_id' => $second_street->id
        ]);

        $repo = new YearRepository(new Year);

        $this->assertCount(2, $repo->getByStreetId($first_street->id));
    }

    public function testIfRepoCanUpdateYear()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $repo = new YearRepository(new Year);
        $repo->update($year, ['year' => '2019']);

        $this->assertEquals('2019', $year->year);
    }

    public function testIfRepoCanDeleteYear()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $repo = new YearRepository(new Year);

        $this->assertTrue($repo->delete($year));
        $this->assertDatabaseMissing('years', ['id' => $year->id]);
    }
}