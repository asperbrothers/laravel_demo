<?php

namespace Tests\Unit\Repository;

use App\Model\City;
use App\Model\Building;
use App\Model\Street;
use App\Model\Year;
use App\Repository\BuildingRepository;
use App\Repository\YearRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuildingRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function testIfBuildingsAssociatedYearExists()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);


        $repo = new BuildingRepository(new Building);
        $building = $repo->getIfExists('Biuro', $city, $street, $year);

        $this->assertNotNull($building);
    }

    public function testIfRepoCanCreateNewRecord()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $repo = new BuildingRepository(new Building);

        $building = $repo->create(['building' => 'Biuro'], $year);

        $this->assertDatabaseHas('buildings', ['id' => $building->id]);
    }

    public function testIfRepoWillReturnDataByGivenCityStreetAndYear()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $second_year = Year::create([
            'year' => '2011',
            'street_id' => $street->id
        ]);

        $first_building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $second_building = Building::create([
            'name' => 'Biuro 2',
            'year_id' => $year->id
        ]);

        $third_building = Building::create([
            'name' => 'Biuro 3',
            'year_id' => $second_year->id
        ]);

        $repo = new BuildingRepository(new Building);

        $properties = [
            'city' => $city->id,
            'street' => $street->id,
            'year' => $year->id
        ];

        $this->assertCount(2, $repo->getByProperties($properties));
    }

    public function testIfRepoCanUpdateBuilding()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $repo = new BuildingRepository(new Building);

        $repo->update($building, ['name' => 'New name']);

        $this->assertEquals('New name', $building->name);
    }

    public function testIfRepoCanDeleteBuilding()
    {
        $city = City::create([
            'name' => 'Warszawa'
        ]);

        $street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2010',
            'street_id' => $street->id
        ]);

        $building = Building::create([
            'name' => 'Biuro',
            'year_id' => $year->id
        ]);

        $repo = new BuildingRepository(new Building);

        $this->assertTrue($repo->delete($building));
        $this->assertDatabaseMissing('buildings', ['id' => $building->id]);
    }
}
