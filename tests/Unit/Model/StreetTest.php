<?php

namespace Tests\Unit\Model;

use App\Model\Street;
use Tests\TestCase;

class StreetTest extends TestCase
{
    public function testIfModelHasRelations()
    {
        $street = new Street();

        $this->assertEquals(
            'Illuminate\Database\Eloquent\Relations\BelongsTo', get_class($street->city())
        );

        $this->assertEquals(
            'Illuminate\Database\Eloquent\Relations\HasMany', get_class($street->years())
        );
    }
}