<?php

namespace Tests\Unit\Model;

use App\Model\Building;
use Tests\TestCase;

class BuildingTest extends TestCase
{
    public function testIfModelHasRelations()
    {
        $building = new Building();

        $this->assertEquals(
            'Illuminate\Database\Eloquent\Relations\BelongsTo', get_class($building->year())
        );
    }
}