<?php

namespace Tests\Unit\Model;

use App\Model\City;
use Tests\TestCase;

class CityTest extends TestCase
{
    public function testIfModelHasRelations()
    {
        $city = new City();

        $this->assertEquals(
            'Illuminate\Database\Eloquent\Relations\HasMany', get_class($city->streets())
        );
    }
}