<?php

namespace Tests\Unit\Model;

use App\Model\Year;
use Tests\TestCase;

class YearTest extends TestCase
{
    public function testIfModelHasRelations()
    {
        $year = new Year();

        $this->assertEquals(
            'Illuminate\Database\Eloquent\Relations\BelongsTo', get_class($year->street())
        );

        $this->assertEquals(
            'Illuminate\Database\Eloquent\Relations\HasMany', get_class($year->buildings())
        );
    }
}