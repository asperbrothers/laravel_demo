<?php

namespace App\Dto;

use App\Enums\XlsxDataHeaders;

class XlsxDto implements FileDtoInterface
{
    private $city;

    private $street;

    private $year;

    private $building;

    /**
     * @param array $data
     * @return $this
     */
    public function map(array $data)
    {
        $this->city = $data[XlsxDataHeaders::CITY];
        $this->street = $data[XlsxDataHeaders::STREET];
        $this->year = $data[XlsxDataHeaders::YEAR];
        $this->building = $data[XlsxDataHeaders::BUILDING];

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * @return string
     */
    public function getBuilding(): string
    {
        return $this->building;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'city' => $this->city,
            'street' => $this->street,
            'year' => $this->year,
            'building' => $this->building
        ];
    }
}