<?php

namespace App\Dto;

interface FileDtoInterface
{
    public function map(array $data);

    public function getCity(): string;

    public function getStreet(): string;

    public function getYear(): string;

    public function getBuilding(): string;

    public function toArray(): array;
}