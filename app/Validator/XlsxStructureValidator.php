<?php

namespace App\Validator;

use App\Enums\XlsxDataHeaders;

class XlsxStructureValidator implements ValidatorInterface
{
    public function validate(array $data): ?bool
    {
        if($this->isValidDataStructure($data)) {
            return true;
        }

        return false;
    }

    private function isValidDataStructure(array $data)
    {
        return isset($data[XlsxDataHeaders::CITY]) && isset($data[XlsxDataHeaders::STREET]) &&
            isset($data[XlsxDataHeaders::YEAR]) && isset($data[XlsxDataHeaders::BUILDING]);
    }
}