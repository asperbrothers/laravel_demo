<?php

namespace App\Enums;

class XlsxDataHeaders
{
    const CITY = 'miasto';
    const STREET = 'ulica';
    const YEAR = 'rok';
    const BUILDING = 'nazwa_obiektu';
}