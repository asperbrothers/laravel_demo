<?php

namespace App\Providers;

use App\Dto\FileDtoInterface;
use App\Dto\XlsxDto;
use App\Repository\BuildingRepository;
use App\Repository\BuildingRepositoryInterface;
use App\Repository\CityRepository;
use App\Repository\CityRepositoryInterface;
use App\Repository\StreetRepository;
use App\Repository\StreetRepositoryInterface;
use App\Repository\YearRepository;
use App\Repository\YearRepositoryInterface;
use App\Validator\ValidatorInterface;
use App\Validator\XlsxStructureValidator;
use Illuminate\Support\ServiceProvider;

class DependencyInjectionProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // repos
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(StreetRepositoryInterface::class, StreetRepository::class);
        $this->app->bind(YearRepositoryInterface::class, YearRepository::class);
        $this->app->bind(BuildingRepositoryInterface::class, BuildingRepository::class);

        // validators
        $this->app->bind(ValidatorInterface::class, XlsxStructureValidator::class);

        // dto
        $this->app->bind(FileDtoInterface::class, XlsxDto::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
