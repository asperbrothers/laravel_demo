<?php

namespace App\Support;

class PathBuilder
{
    public static function createFilePath(string $filename): string
    {
        return sprintf("%s/%s/%s",
            public_path(),
            config('custom.imports_folder'),
            $filename
        );
    }
}