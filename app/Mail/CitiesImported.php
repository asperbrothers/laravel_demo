<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CitiesImported extends Mailable
{
    use Queueable, SerializesModels;

    protected $added_records = 0;

    protected $modified_records = 0;

    /**
     * CitiesImported constructor.
     * @param int $added_records
     * @param int $modified_records
     */
    public function __construct(int $added_records, int $modified_records)
    {
        $this->added_records = $added_records;
        $this->modified_records = $modified_records;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_SENDER_ADDRESS'), env('APP_NAME'))
            ->subject('Dane zostały zaimportowane.')
            ->to(config('custom.admin_email'))
            ->markdown('emails.cities_imported', [
                'added_records' => $this->added_records,
                'modified_records' => $this->modified_records
            ]);
    }
}
