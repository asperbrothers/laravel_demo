<?php

namespace App\Console\Commands;

use App\Imports\CitiesImport;
use App\Mail\CitiesImported;
use App\Manager\ImportManager;
use App\Support\PathBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class SaveCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cities:save {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Saving data from xlsx to the database';

    protected $excel;

    protected $importManager;

    protected $added_records = 0;

    protected $modified_records = 0;

    /**
     * SaveCities constructor.
     * @param Excel $excel
     * @param ImportManager $importManager
     */
    public function __construct(Excel $excel, ImportManager $importManager)
    {
        parent::__construct();

        $this->excel = $excel;
        $this->importManager = $importManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');

        if(! file_exists(PathBuilder::createFilePath($filename))) {
            $this->error('Requested file does not exist!');
            return false;
        }

        try {
            $this->createRecords($filename);
            // Send mail to admin with status
            Mail::send(new CitiesImported($this->added_records, $this->modified_records));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function createRecords(string $filename) : void
    {
        $data = $this->excel::toArray(new CitiesImport, $filename, 'imports');

        foreach($data as $rows) {
            foreach($rows as $row) {
                $saved = $this->importManager->save($row);
                $this->added_records += $saved ? 1 : 0;
            }
        }
        
        $this->line(sprintf("Added %d records.", $this->added_records));
    }
}
