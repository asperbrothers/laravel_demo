<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = ['name', 'year_id'];

    public function year()
    {
        return $this->belongsTo(Year::class);
    }
}
