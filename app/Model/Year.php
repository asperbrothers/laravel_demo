<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Year extends Model
{
    protected $fillable = ['year', 'street_id'];

    public function street()
    {
        return $this->belongsTo(Street::class);
    }

    public function buildings() : HasMany
    {
        return $this->hasMany(Building::class);
    }
}
