<?php

namespace App\Manager;

use App\Dto\FileDtoInterface;
use App\Dto\XlsxDto;
use App\Exceptions\InvalidFileStructureFormatException;
use App\Model\City;
use App\Model\Building;
use App\Model\Street;
use App\Model\Year;
use App\Repository\BuildingRepositoryInterface;
use App\Repository\CityRepositoryInterface;
use App\Repository\StreetRepositoryInterface;
use App\Repository\YearRepositoryInterface;
use App\Validator\ValidatorInterface;

class ImportManager
{
    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * @var StreetRepositoryInterface
     */
    private $streetRepository;

    /**
     * @var YearRepositoryInterface
     */
    private $yearRepository;

    /**
     * @var BuildingRepositoryInterface
     */
    private $buildingRepository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var FileDtoInterface
     */
    private $dto;

    /**
     * @var bool
     */
    private $is_new_record = false;

    /**
     * ImportManager constructor.
     * @param CityRepositoryInterface $cityRepository
     * @param StreetRepositoryInterface $streetRepository
     * @param YearRepositoryInterface $yearRepository
     * @param BuildingRepositoryInterface $buildingRepository
     * @param ValidatorInterface $validator
     * @param FileDtoInterface $dto
     */
    public function __construct(
        CityRepositoryInterface $cityRepository,
        StreetRepositoryInterface $streetRepository,
        YearRepositoryInterface $yearRepository,
        BuildingRepositoryInterface $buildingRepository,
        ValidatorInterface $validator,
        FileDtoInterface $dto
    )
    {
        $this->cityRepository = $cityRepository;
        $this->streetRepository = $streetRepository;
        $this->yearRepository = $yearRepository;
        $this->buildingRepository = $buildingRepository;
        $this->validator = $validator;
        $this->dto = $dto;
    }

    /**
     * @param array $row
     * @return bool
     * @throws InvalidFileStructureFormatException
     */
    public function save(array $row): bool
    {
        $this->is_new_record = false;

        if($this->validator->validate($row) === false) {
            throw new InvalidFileStructureFormatException('Invalid data structure.');
        }

        $record = $this->dto->map($row);

        $city = $this->createCity($record);
        $street = $this->createStreet($record, $city);
        $year = $this->createYear($record, $city, $street);
        $this->createBuilding($record, $city, $street, $year);

        return $this->is_new_record;
    }

    /**
     * @param FileDtoInterface $record
     * @return City
     */
    private function createCity(FileDtoInterface $record): City
    {
        if(! $city = $this->cityRepository->getIfExists($record->getCity())) {
            $this->is_new_record = true;
            $city = $this->cityRepository->create($record->toArray());
        }

        return $city;
    }

    /**
     * @param FileDtoInterface $record
     * @param City $city
     * @return Street
     */
    private function createStreet(FileDtoInterface $record, City $city): Street
    {
        if(! $street = $this->streetRepository->getIfExists($record->getStreet(), $city)) {
            $this->is_new_record = true;
            $street = $this->streetRepository->create($record->toArray(), $city);
        }

        return $street;
    }

    /**
     * @param FileDtoInterface $record
     * @param City $city
     * @param Street $street
     * @return Year
     */
    private function createYear(FileDtoInterface $record, City $city, Street $street): Year
    {
        if(! $year = $this->yearRepository->getIfExists($record->getYear(), $city, $street)) {
            $this->is_new_record = true;
            $year = $this->yearRepository->create($record->toArray(), $street);
        }

        return $year;
    }

    /**
     * @param FileDtoInterface $record
     * @param City $city
     * @param Street $street
     * @param Year $year
     * @return Building
     */
    private function createBuilding(FileDtoInterface $record, City $city, Street $street, Year $year): Building
    {
        if(! $building = $this->buildingRepository->getIfExists($record->getBuilding(), $city, $street, $year)) {
            $this->is_new_record = true;
            $building = $this->buildingRepository->create($record->toArray(), $year);
        }

        return $building;
    }
}
