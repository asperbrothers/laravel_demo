<?php

namespace App\Repository;

use App\Model\City;
use App\Model\Building;
use App\Model\Street;
use App\Model\Year;
use Illuminate\Support\Collection;

class BuildingRepository implements BuildingRepositoryInterface
{
    /**
     * @var Building
     */
    private $model;

    /**
     * BuildingRepository constructor.
     * @param Building $model
     */
    public function __construct(Building $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $name
     * @param City $city
     * @param Street $street
     * @param Year $year
     * @return Building|null
     */
    public function getIfExists(string $name, City $city, Street $street, Year $year): ?Building
    {
        $buildingObj = \DB::table('buildings')
            ->select(
                'buildings.id as building_id', 'buildings.name as building_name', 'cities.name as city_name', 'years.year',
                'years.id as year_id', 'streets.name as street_name'
            )
            ->join('years', 'buildings.year_id', '=', 'years.id')
            ->join('streets', 'years.street_id', '=', 'streets.id')
            ->join('cities', 'streets.city_id', '=', 'cities.id')
            ->where('cities.id', $city->id)
            ->where('streets.id', $street->id)
            ->where('years.id', $year->id)
            ->where('buildings.name', $name)
            ->first();

        if(!$buildingObj) {
            return $buildingObj;
        }

        $building = new Building([
            'name' => $buildingObj->building_name,
            'year_id' => $buildingObj->year_id
        ]);

        $building->id = $buildingObj->building_id;

        return $building;
    }

    /**
     * @param array $data
     * @param Year $year
     * @return Building|null
     */
    public function create(array $data, Year $year): ?Building
    {
        return $this->model->create([
            'name' => $data['building'],
            'year_id' => $year->id
        ]);
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function getByProperties(array $data): Collection
    {
        return \DB::table('buildings')
            ->select('buildings.name as building_name', 'cities.name as city_name', 'years.year', 'streets.name as street_name')
            ->leftJoin('years', 'buildings.year_id', '=', 'years.id')
            ->leftJoin('streets', 'years.street_id', '=', 'streets.id')
            ->leftJoin('cities', 'streets.city_id', '=', 'cities.id')
            ->where('city_id', $data['city'])
            ->where('street_id', $data['street'])
            ->where('year_id', $data['year'])
            ->get();
    }

    /**
     * @param Building $building
     * @param array $data
     * @return Building
     */
    public function update(Building $building, array $data): Building
    {
        $building->update([
             'name' => $data['name']
        ]);

        return $building;
    }

    /**
     * @param Building $building
     * @return bool
     * @throws \Exception
     */
    public function delete(Building $building): bool
    {
        return $building->delete();
    }
}
