<?php

namespace App\Repository;

use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use Illuminate\Database\Eloquent\Collection;

class YearRepository implements YearRepositoryInterface
{
    /**
     * @var Year
     */
    private $model;

    /**
     * YearRepository constructor.
     * @param Year $model
     */
    public function __construct(Year $model)
    {
        $this->model = $model;
    }

    /**
     * @param $year
     * @param City $city
     * @param Street $street
     * @return Year|null
     */
    public function getIfExists($year, City $city, Street $street): ?Year
    {
        $yearObj = \DB::table('years')
            ->select('years.id', 'years.year', 'years.street_id')
            ->join('streets', 'years.street_id', '=', 'streets.id')
            ->join('cities', 'streets.city_id', '=', 'cities.id')
            ->where('year', (int) $year)
            ->where('city_id', $city->id)
            ->where('street_id', $street->id)
            ->first();

        if(!$yearObj) {
            return $yearObj;
        }

        $year = new Year((array) $yearObj);
        $year->id = $yearObj->id;

        return $year;
    }

    /**
     * @param array $data
     * @param Street $street
     * @return Year|null
     */
    public function create(array $data, Street $street): ?Year
    {
        return $this->model->create([
            'year' => (int) $data['year'],
            'street_id' => $street->id
        ]);
    }

    /**
     * @param int $street_id
     * @return Collection
     */
    public function getByStreetId(int $street_id): Collection
    {
        return $this->model->where('street_id', $street_id)->get();
    }

    /**
     * @param Year $year
     * @param array $data
     * @return Year
     */
    public function update(Year $year, array $data): Year
    {
        $year->update([
            'year' => $data['year']
        ]);

        return $year;
    }

    /**
     * @param Year $year
     * @return bool
     * @throws \Exception
     */
    public function delete(Year $year): bool
    {
        $year->buildings()->delete();

        return $year->delete();
    }
}