<?php

namespace App\Repository;

use App\Model\City;
use App\Model\Street;
use Illuminate\Database\Eloquent\Collection;

class StreetRepository implements StreetRepositoryInterface
{
    /**
     * @var Street
     */
    private $model;

    /**
     * StreetRepository constructor.
     * @param Street $model
     */
    public function __construct(Street $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $name
     * @param City $city
     * @return Street|null
     */
    public function getIfExists(string $name, City $city): ?Street
    {
        return $this->model->where('name', $name)
            ->where('city_id', $city->id)
            ->first();
    }

    /**
     * @param array $data
     * @param City $city
     * @return Street|null
     */
    public function create(array $data, City $city): ?Street
    {
        return $this->model->create([
            'name' => $data['street'],
            'city_id' => $city->id
        ]);
    }

    /**
     * @param int $city_id
     * @return Collection
     */
    public function getByCityId(int $city_id): Collection
    {
        return $this->model->where('city_id', $city_id)->get();
    }

    /**
     * @param Street $street
     * @param array $data
     * @return Street
     */
    public function update(Street $street, array $data): Street
    {
        $street->update([
            'name' => $data['name']
        ]);

        return $street;
    }

    /**
     * @param Street $street
     * @return bool
     * @throws \Exception
     */
    public function delete(Street $street): bool
    {
        $years = $street->years()->get();

        if($years->count() > 0) {
            foreach($years as $year) {
                $year->buildings()->delete();
            }

            $street->years()->delete();
        }

        return $street->delete();
    }
}