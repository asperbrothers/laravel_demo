<?php

namespace App\Repository;

use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use Illuminate\Database\Eloquent\Collection;

interface YearRepositoryInterface
{
    public function getIfExists($year, City $city, Street $street): ?Year;

    public function create(array $data, Street $street): ?Year;

    public function getByStreetId(int $street_id): Collection;

    public function update(Year $year, array $data): Year;

    public function delete(Year $year): bool;
}