<?php

namespace App\Repository;

use App\Model\City;
use App\Model\Street;
use Illuminate\Database\Eloquent\Collection;

interface StreetRepositoryInterface
{
    public function getIfExists(string $name, City $city): ?Street;

    public function create(array $data, City $city): ?Street;

    public function getByCityId(int $city_id): Collection;

    public function update(Street $street, array $data): Street;

    public function delete(Street $street): bool;
}