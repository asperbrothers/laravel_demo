<?php

namespace App\Repository;

use App\Model\City;
use Illuminate\Database\Eloquent\Collection;

class CityRepository implements CityRepositoryInterface
{
    /**
     * @var City
     */
    private $model;

    /**
     * CityRepository constructor.
     * @param City $model
     */
    public function __construct(City $model)
    {
        $this->model = $model;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param string $name
     * @return City|null
     */
    public function getIfExists(string $name): ?City
    {
        return $this->model->where('name', $name)->first();
    }

    /**
     * @param array $data
     * @return City|null
     */
    public function create(array $data): ?City
    {
        return $this->model->create(['name' => $data['city']]);
    }
}