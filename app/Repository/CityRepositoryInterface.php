<?php

namespace App\Repository;

use App\Model\City;
use Illuminate\Database\Eloquent\Collection;

interface CityRepositoryInterface
{
    public function getAll(): Collection;

    public function getIfExists(string $name): ?City;

    public function create(array $data): ?City;
}