<?php

namespace App\Repository;

use App\Model\Building;
use App\Model\City;
use App\Model\Street;
use App\Model\Year;
use Illuminate\Support\Collection;

interface BuildingRepositoryInterface
{
    public function getIfExists(string $name, City $city, Street $street, Year $year): ?Building;

    public function create(array $data, Year $year): ?Building;

    public function getByProperties(array $data): Collection;

    public function update(Building $building, array $data): Building;

    public function delete(Building $building): bool;
}