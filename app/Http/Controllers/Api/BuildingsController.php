<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\BuildingUpdateRequest;
use App\Http\Resources\BuildingResource;
use App\Model\Building;
use App\Repository\BuildingRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BuildingsController
{
    /**
     * @var BuildingRepositoryInterface
     */
    private $buildingRepository;

    /**
     * BuildingsController constructor.
     * @param BuildingRepositoryInterface $buildingRepository
     */
    public function __construct(BuildingRepositoryInterface $buildingRepository)
    {
        $this->buildingRepository = $buildingRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBuildings(Request $request): JsonResponse
    {
        return response()->json(['data' => $this->buildingRepository->getByProperties($request->all())]);
    }

    /**
     * @param Building $building
     * @param BuildingUpdateRequest $request
     * @return BuildingResource
     */
    public function update(Building $building, BuildingUpdateRequest $request): BuildingResource
    {
        $building = $this->buildingRepository->update($building, $request->all());

        return new BuildingResource($building);
    }

    /**
     * @param Building $building
     * @return JsonResponse
     */
    public function delete(Building $building): JsonResponse
    {
        $this->buildingRepository->delete($building);

        return response()->json(['status' => '200', 'message' => 'Building has been removed.']);
    }
}
