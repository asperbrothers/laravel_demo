<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StreetUpdateRequest;
use App\Http\Resources\StreetResource;
use App\Model\Street;
use App\Repository\StreetRepositoryInterface;
use Illuminate\Http\JsonResponse;

final class StreetsController
{
    /**
     * @var StreetRepositoryInterface
     */
    private $streetRepository;

    /**
     * StreetsController constructor.
     * @param StreetRepositoryInterface $streetRepository
     */
    public function __construct(StreetRepositoryInterface $streetRepository)
    {
        $this->streetRepository = $streetRepository;
    }

    /**
     * @param int $city_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByCity(int $city_id): JsonResponse
    {
        return response()->json(['data' => $this->streetRepository->getByCityId($city_id)]);
    }

    /**
     * @param Street $street
     * @param StreetUpdateRequest $request
     * @return StreetResource
     */
    public function update(Street $street, StreetUpdateRequest $request): StreetResource
    {
        $street = $this->streetRepository->update($street, $request->all());

        return new StreetResource($street);
    }

    /**
     * @param Street $street
     * @return JsonResponse
     */
    public function delete(Street $street): JsonResponse
    {
        $this->streetRepository->delete($street);

        return response()->json(['status' => '200', 'message' => 'Street has been removed.']);
    }
}