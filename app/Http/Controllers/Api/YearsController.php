<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\YearUpdateRequest;
use App\Http\Resources\YearResource;
use App\Model\Year;
use App\Repository\YearRepositoryInterface;
use Illuminate\Http\JsonResponse;

final class YearsController
{
    /**
     * @var YearRepositoryInterface
     */
    private $yearRepository;

    /**
     * YearsController constructor.
     * @param YearRepositoryInterface $yearRepository
     */
    public function __construct(YearRepositoryInterface $yearRepository)
    {
        $this->yearRepository = $yearRepository;
    }

    /**
     * @param int $street_id
     * @return JsonResponse
     */
    public function getByStreet(int $street_id): JsonResponse
    {
        return response()->json(['data' => $this->yearRepository->getByStreetId($street_id)]);
    }

    /**
     * @param Year $year
     * @param YearUpdateRequest $request
     * @return YearResource
     */
    public function update(Year $year, YearUpdateRequest $request): YearResource
    {
        $year = $this->yearRepository->update($year, $request->all());

        return new YearResource($year);
    }

    /**
     * @param Year $year
     * @return JsonResponse
     */
    public function delete(Year $year): JsonResponse
    {
        $this->yearRepository->delete($year);

        return response()->json(['status' => '200', 'message' => 'Year has been removed.']);
    }
}