<?php

namespace App\Http\Controllers\Site;

use App\Exceptions\InvalidFileFormatException;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImportCitiesRequest;
use App\Repository\CityRepositoryInterface;
use App\Support\PathBuilder;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class CitiesController extends Controller
{
    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * CitiesController constructor.
     * @param CityRepositoryInterface $cityRepository
     */
    public function __construct(CityRepositoryInterface $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        return view('site.cities.index', [
            'cities' => $this->cityRepository->getAll()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import(): View
    {
        return view('site.cities.import');
    }

    /**
     * @param ImportCitiesRequest $request
     * @return $this
     * @throws InvalidFileFormatException
     */
    public function importSave(ImportCitiesRequest $request)
    {
        $file = Storage::disk('imports')->put('', $request->file('import_file'));

        if(! file_exists(PathBuilder::createFilePath($file))) {
            throw new InvalidFileFormatException('Invalid file format.');
        }

        Artisan::call('cities:save', [
            'filename' => $file
        ]);

        return redirect()->route('cities.index')->with('success', 'File has been successfuly imported.');
    }
}
